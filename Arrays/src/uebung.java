/**
  *
  * Beschreibung
  *  Einf�hrung in die Programmierung mit Arrays 
  * @version 1.0 vom 19.03.2017
  * @author 
  */
import java.util.Scanner;
public class uebung {
  
  public static void main(String[] args) {
    
    Scanner sc = new Scanner (System.in);
    
    //Deklaration eines Arrays, 5 integer Werte
    //Name des Arrays: "intArray"
    int [] intArray = new int [5];
    
    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    intArray [2] = 1000;  //es geht von 0-3, also 4 Stellen 
    intArray [4] = 500;
    
    System.out.println("Index 0: " + intArray[0]);
    System.out.println("Index 1: " + intArray[1]);
    System.out.println("Index 2: " + intArray[2]);
    System.out.println("Index 3: " + intArray[3]);
    System.out.println("Index 4: " + intArray[4]);
    
    //Array ausgeben mit For-Schleife
    for (int i = 0; i < intArray.length; i++) {
    	System.out.println("Index " + i + ". " + intArray);
    }
    
    //Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // L�nge 3, mit 3 double Werten 
    
    
    
    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
    
    
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
    System.out.println("An welchen Index soll der neue Wert?");
    int index = sc.nextInt(); 
    
    System.out.println("Geben Sie den neuen Wert f�r index " +   index   + " an:");
    int wert = sc.nextInt(); 
    
    intArray[index] = wert; 
    
    
    //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
    // __   __  __  __  __  
    
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //1. alle Felder sollen den Wert 0 erhalten
    
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //2. alle Felder sollen vom Nutzer einen Wert bekommen
          //nutzen Sie dieses Mal die Funktion �length�
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
           //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    
    
    
    
    
    
  } // end of main
  
} // end of class arrayEinfuehrung
