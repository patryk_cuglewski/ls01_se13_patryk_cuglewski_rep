package variablen_ueberfall;

public class variablen_ueberfall {		//Klassenname normalerweise gro� schreiben

	public static void main(String[] args) {		//main Methode, Einsprungspunkt bei der Ausf�hrung
		// TODO Auto-generated method stub

		
		//Variablendeklaration (Erstellung einer Variablen, Datentyp wird zugewiesen)
		//Variablennamen immer klein schreiben!
		int meinVermoegen;  //Deklaration einer ganzzahligen Variablen (Datentyp integer)
		
		//Initalisierung der Variablen (Startwert wird zugewiesen)
		meinVermoegen = 20;
		
		//Deklaration und Initalisierung kann auc in einem Schritt erfolgen
		int robisVermoegen = 4000;
		
		/*Berechnung
		 Dabei ist das "=" ein Zuweisungoperator: der Wert auf der rechte Seite 
		 wird der Varbiablen auf der linken Seite zugewiesen. ALso erst erfolgt die 
		 Berechnung, dann wird das Ergebnis in der Varbiablen speichern */
		meinVermoegen = meinVermoegen + 100; //neuer Wert von "meinVermoegen": 120
		
		/*Ausgabe vom Text "meinVermoegen:" und vom Wert der
		 Variablen meinVermoegen, also momentan 120
		 System.out.println: Befehl, um eine Bildschirmausgabe mit einem Zeilenumbruch zu erzeugen */
		System.out.println("meinVermoegen: " + meinVermoegen);
		
		/*Ausgabe vom Text "robisVermoegen:" und vom Wert der
		  Variablen meinVermoegen, also momentan 4000*/
		System.out.println("robisVermoegen: " + robisVermoegen);
		
		//"�berfall"
		//Rechte Seite vom = : Berechnung. Ergennis wird in Variablen aus linker Seite geschrieben
		meinVermoegen = meinVermoegen + robisVermoegen; //neuer Wert: 120 + 4000
		
		//Robi wurde ausgeraubt, sein Verm�gen wird auf 0 gesetzt 
		robisVermoegen = 0;
		
		System.out.println("meinVermoegen: " + meinVermoegen); //4120
		System.out.println("robisVermoegen: " + robisVermoegen); //0
		
		
	} //end of main

} //end of class variablen
