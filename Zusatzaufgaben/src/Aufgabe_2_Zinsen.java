import java.util.Scanner;

public class Aufgabe_2_Zinsen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner tastatur = new Scanner(System.in);
		
		//1. Deklarieren
		
		double einzahlungEuro;
		double zinsenProzent;
		double zinsenEuro;
		double jahre;
		double geldInvestor;
		
		//2. Einzahlung in Euro
		
		System.out.print("Einzahlung in Euro: ");
		einzahlungEuro = tastatur.nextDouble();
		
		//Zinsen in Prozent
		
		System.out.print("Zinsen in Prozent: ");
		zinsenProzent = tastatur.nextDouble();
		
		// �ber wie viele Jahre?
		
		System.out.print("Zahlung �ber Jahre: ");
		jahre = tastatur.nextDouble();
		
		//3. Geld f�r den Investor
		
		geldInvestor = einzahlungEuro * zinsenProzent /100 * jahre;
		System.out.print("Der Investor hat insgesamt ");
		System.out.printf("%.2f", geldInvestor);
		System.out.print(" Euro erhalten.");
		
	}

}
