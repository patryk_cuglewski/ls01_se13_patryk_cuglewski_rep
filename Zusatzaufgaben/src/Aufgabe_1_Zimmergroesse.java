import java.util.Scanner;

public class Aufgabe_1_Zimmergroesse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner tastatur = new Scanner(System.in);
		
		//1. Variablen deklarieren
		
		double laengeMeter;
		double breiteMeter;
		double quadratMeter;
		
		//2. Eingabe L�nge
		
		System.out.print("L�nge (m): ");
		laengeMeter = tastatur.nextDouble();
		
		//Eingabe Breite
		
		System.out.print("Breite (m): ");
		breiteMeter = tastatur.nextDouble();
		
		//Quadratmeter
		quadratMeter = laengeMeter * breiteMeter;
		System.out.print("Das Zimmer hat ");
		System.out.print(quadratMeter);
		System.out.println(" qm.");
	}

}
