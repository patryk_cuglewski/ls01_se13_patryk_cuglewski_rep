/* Operatoren.java
   Uebung zu Operatoren in Java
*/
public class operatoren {
     public static void main(String[] args) {
            /* 1. Deklarieren Sie zwei Ganzzahlen.*/

    	  	int zahl1;
    	  	int zahl2;
            
    	  	
            /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
                  und geben Sie sie auf dem Bildschirm aus. */
    	  	
    	  	zahl1 = 75;
    	  	zahl2 = 23;
    	  			
    	  	System.out.println(zahl1);
    	  	System.out.println(zahl2);
          
            /* 3. Addieren Sie die Ganzzahlen
                  und geben Sie das Ergebnis auf dem Bildschirm aus. */
    	  			
    	  	System.out.println("75 + 23 = " + zahl1 + zahl2);
    	  	
            /* 4. Wenden Sie *alle anderen* arithmetischen Operatoren auf die
                  Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
                  Bildschirm aus. */
    	  	
    	  	System.out.println("75 - 23 = " + (zahl1 - zahl2));
    	  	System.out.println("75 : 23 = " + zahl1 / zahl2);
    	  	System.out.println("75 x 23 = " + zahl1 * zahl2);

            /* 5. Ueberpruefen Sie, ob die beiden Ganzzahlen gleich sind
                  und geben Sie das Ergebnis auf dem Bildschirm aus. */
    	  	
    	  	System.out.println("75 = 23? = " + (zahl1 == zahl2));

            /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
                  und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
    	  	
    	  	System.out.println("23 < 75? = " + (zahl2<zahl1));
    	  	System.out.println("75 < 23? = " + (zahl1<zahl2));

            /* 7. Ueberpruefen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
                  und geben Sie das Ergebnis auf dem Bildschirm aus.
        
                  Tipp: Auch das geht nur mit Operatoren!
            */

    	  	System.out.println(zahl1 <= 50 && zahl1 >= 0);
    	  	
    	  	
      } //Ende von main
}// Ende von Operatoren