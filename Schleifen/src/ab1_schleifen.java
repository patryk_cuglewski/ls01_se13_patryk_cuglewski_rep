import java.util.Scanner;
import java.lang.Math;
public class ab1_schleifen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Aufgabe 1: Z�hlen
				
				int n = 1;
				Scanner sc = new Scanner (System.in);
				System.out.print("Bitte geben Sie n ein: ");
				n = sc.nextInt();
				
				//Heraufz�hlend
				for(int i = 1; i <= n; i++)
				//for(int i = 1; i < 10;i++) {}
					//int i = 1; definiert und deklariert i 
					// i < 10; bestimmt die Bedingung, wie lange die for-Schleife gehen soll 
					// i++ addiert jede Wiederholung i + 1
				{
					
					System.out.print(i);
					if (i >= n )
					{
						System.out.print(" \n");
					}
					else
					{
						System.out.print(", ");
					}
				
				}
				//Hinunterz�hlend
						
				for(int i = 1; i <= n; n--)
				{
					System.out.print(n);
					if (n <= i)
					{
						System.out.print(" ");
					}
					else
					{
						System.out.print(", ");
					}				
				}
		
	}

}
