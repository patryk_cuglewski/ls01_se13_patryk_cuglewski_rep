import  java.util.Scanner;
public class Wiederholung {

		//Dekaration einer Methode
		  
		  public static int quadrieren (int m_ganzeZahl){
		       int ergebnis;
		       ergebnis =  m_ganzeZahl * m_ganzeZahl;
		       return ergebnis;
		    } //end of quadrieren
		  
		  
		  public static int multiplizieren (int m_zahl, int m_zahl2){
		       int ergebnis; 
		       ergebnis = m_zahl * m_zahl2;
		       return ergebnis;
		    
		    } //end of multiplizieren
		  
		  
		  
		  public static void main(String[] args) {
		    
		    //Deklaration eines Scanners
		    Scanner myScanner = new Scanner(System.in);
		    
		    //Deklaration weiterer Variablen
		    //double  doubleVariable; 
		    //String meinText;
		    //char  zeichenerg;
		    int ganzeZahl, erg, ganzeZahl2;
		  
		    
		    System.out.println("Geben Sie eine ganze Zahl ein");
		    ganzeZahl = myScanner.nextInt();
		    
		    System.out.println("Geben Sie zweite ganze Zahl ein");
		    ganzeZahl2 = myScanner.nextInt();
		    
		    erg = quadrieren(ganzeZahl);
		    System.out.println(ganzeZahl + " hoch 2 = " + erg );
		    
		    erg = multiplizieren(ganzeZahl, ganzeZahl2);
		    System.out.println(ganzeZahl + " * " + ganzeZahl2 + " = " + erg );
		    
		    
		
		    
		  } // end of main
		  
		  
		  
		  

		} // end of class Wdh

