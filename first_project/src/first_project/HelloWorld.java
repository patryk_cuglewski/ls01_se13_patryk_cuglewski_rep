package first_project;

public class HelloWorld {

	public static void main(String[] args) {
		/* Dies ist 
		 * ein Mehrzeiliger
		 * Kommentar
		 */
		
		System.out.println("Hello World");
		System.out.print("Guten Tag\n");
	
		
		//print erzeugt Ausgabe ohne Zeilenumbruch 
		System.out.print("Heute ist Montag\n");
		//println: Ausgabe mit Zeilenumbruch 
		System.out.println("Be Happy");
		
		
	
		
		//Klausur!!!!!
		
		
		
		System.out.println(127);
		System.out.println(127 + " Hallo");
		
		//String: Datentyp: Zeichenkette/W�rter, nicht zum Rechnen
		
		System.out.println("127");
		
		//Variable deklariert: vom Typ String, mit Namen text
		//Varibale gleichzeitig initialisiert: Der Variable wird ein Wert gegeben
		String text = "trallalla";
		
		String text2;	//Deklaration
		text2 = " moin"; //Initialisierung
		
		System.out.println(text + text2); //Anwendung beider Strings
		System.out.println(text2 + " was geht?"); //Kombination von 2 Sachen
		
		
		
		//Integer: ganze Zahlen
		int zahl1 = 345, zahl2 = 5; //camel case	//Java ist camel sensitive
		int ergebnis = zahl1+zahl2; //ein Ergenis bilden
		System.out.print("beide Zahlen zusammen: ");
		System.out.println(zahl1+zahl2); //beide Zahlen rechnen
		System.out.println("Das Ergebnis beider Zahlen: " + ergebnis); // Ergebnis beider deklarierter Zahlen ausgeben lassen
		
	}

}
