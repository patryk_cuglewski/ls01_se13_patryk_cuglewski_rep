import java.util.Scanner;

class automat_probe
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       int anzahlTickets; 
       
       // Tickets
       //-----------
       System.out.print("Ticketspreis (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();

       System.out.print("Anzahl der Tickets: ");
       anzahlTickets = tastatur.nextInt(); 
       double gesamtpreis = zuZahlenderBetrag * anzahlTickets;
      
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < gesamtpreis)
       {
    	   System.out.printf("Noch zu zahlen: ");
    	   System.out.printf("%.2f",anzahlTickets * zuZahlenderBetrag - eingezahlterGesamtbetrag);
    		System.out.println(" Euro");
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - gesamtpreis;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von " + "%.2f" ,r�ckgabebetrag);
    	   System.out.println(" EURO");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println(" EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
       tastatur.close();
    }
}

		//Double "gesamtpreis" fasst die Rechnung "zuZahlenderBetrag * anzahlTickets" zusammen und hilft uns die Anzahl der Tickets in die Rechnung zu integrieren.
		//Ich habe mich f�r die Anzahl der Tickets f�r einen int Datentyp entschieden, da man nur ganze Tickets bestellen kann und nicht 2 und ein halbes.
		//Der "gesamtpreis" wurde in der Rechnung f�r den "Geldeinwurf" und "R�ckgeldberechnung und -Ausgabe" integriert.