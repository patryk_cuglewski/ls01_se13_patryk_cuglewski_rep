import java.util.Scanner;

class Fahrkartenautomat_Arrays {

	 public static void main(String[] args) {

		 do {
		 
		//Fahrscheinerfassung 
	    	String[] Fahrkarten = {"Einzelfahrschein AB", "Einzelfahrschein BC", "Einzelfahrschein ABC, Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte AB", "Kleingruppen-Tageskarte BC", "Kleingruppen-Tageskarte ABC"};
			double[] Fahrpreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
			
			   double gesamtpreis = 0; 
		       double eingezahlterGesamtbetrag = 0;
		 	   double eingeworfeneM�nze = 0;
		 	   double r�ckgabebetrag = 0;
		 	   double fahrscheinpreis = 0;
		     
		 	  double TicketMinimum = 1;
		 	  double TicketMaximum = 10;
		     

				//Fahrscheinerfassung 
				
				fahrscheinpreis = FahrscheinartErfassen (fahrscheinpreis, Fahrkarten, Fahrpreise);
				
				//Erfassung Anzahl Tickets, Berechnung des Gesamtpreises
			
			    gesamtpreis = fahrkartenbestellungErfassen(fahrscheinpreis, gesamtpreis, TicketMinimum, TicketMaximum);
			    
			    
			    //Bezahlung der Fahrkarten, Geldeinwurf
			  
			   r�ckgabebetrag = fahrkartenBezahlen (gesamtpreis, eingezahlterGesamtbetrag, eingeworfeneM�nze, r�ckgabebetrag);
			    
			   // Fahrscheinausgabe
			   fahrkartenAusgeben ();
			   
			   // R�ckgeldberechnung und -Ausgabe
			   rueckgeldAusgeben (r�ckgabebetrag);
		 }
		 while (true);
	    
	    }
	
		 
	 public static double FahrscheinartErfassen (double fahrscheinpreis, String[]Fahrscheine, double[]Fahrpreis) {
			Scanner Fahrscheinauswahl = new Scanner (System.in);
			System.out.println("Welche Art von Fahrschein m�chten Sie kaufen? "
					+ "\n 1) Einzelfahrschein Berlin AB"
					+ "\n 2) Einzehlfahrschein Berlin BC"
					+ "\n 3) Einzelfahrschein ABC"
					+ "\n 4) Kurzstrecke"
					+ "\n 5) Tageskarte Berlin AB"
					+ "\n 6) Tageskarte Berlin BC"
					+ "\n 7) Tageskarte Berlin ABC"
					+ "\n 8) Kleingruppen-Tageskarten AB"
					+ "\n 9) Kleingruppen-Tageskarte BC"
					+ "\n 10) Kleingruppen-Tageskarte ABC");
			String Fahrscheinart = Fahrscheinauswahl.next();
			if (Fahrscheinart.equals("1")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[0] + " gew�hlt, der Preis betr�gt "+ "2.90�");
				fahrscheinpreis = Fahrpreis[0];
				return fahrscheinpreis;
			}
			if (Fahrscheinart.equals("2")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[1] + " gew�hlt, der Preis betr�gt "+ "3.30�");
				fahrscheinpreis = Fahrpreis[1];
				return fahrscheinpreis;
			}
			if (Fahrscheinart.equals("3")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[2] + " gew�hlt, der Preis betr�gt "+ "3.60�");
				fahrscheinpreis = Fahrpreis[2];
				return fahrscheinpreis;
			}
			if (Fahrscheinart.equals("4")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[3] + " gew�hlt, der Preis betr�gt "+ "1.90�");
				fahrscheinpreis = Fahrpreis[3];
				return fahrscheinpreis;
			}
			if (Fahrscheinart.equals("5")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[4] + " gew�hlt, der Preis betr�gt "+ "8.60�");
				fahrscheinpreis = Fahrpreis[4];
				return fahrscheinpreis;
			}
			if (Fahrscheinart.equals("6")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[5] + " gew�hlt, der Preis betr�gt "+ "9.00�");
				fahrscheinpreis = Fahrpreis[5];
				return fahrscheinpreis;
			}
			if (Fahrscheinart.equals("7")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[6] + " gew�hlt, der Preis betr�gt "+ "9.60�");
				fahrscheinpreis = Fahrpreis[6];
				return fahrscheinpreis;
			}
			if (Fahrscheinart.equals("8")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[7] + " gew�hlt, der Preis betr�gt "+ "23.50�");
				fahrscheinpreis = Fahrpreis[7];
				return fahrscheinpreis;
			}
			if (Fahrscheinart.equals("9")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[8] + " gew�hlt, der Preis betr�gt "+ "24.30�");
				fahrscheinpreis = Fahrpreis[8];
				return fahrscheinpreis;
			}
			if (Fahrscheinart.equals("10")) {
				System.out.println("Sie haben eine/n " + Fahrscheine[9] + " gew�hlt, der Preis betr�gt "+ "24.90�");
				fahrscheinpreis = Fahrpreis[9];
				return fahrscheinpreis;
			}
			
			
			return fahrscheinpreis;
			
	    }
	       
	 
    public static double fahrkartenbestellungErfassen(double fahrscheinpreis, double gesamtpreis, double TicketMinimum, double TicketMaximum) {

    	Scanner fahrkartenanzahl = new Scanner (System.in);
		System.out.println("Wie viele Fahrkarten m�chten Sie kaufen?\n");
		double anzahlfahrkarten = fahrkartenanzahl.nextDouble();
			System.out.println("Anzahl der Tickets: " + anzahlfahrkarten);
		gesamtpreis = fahrscheinpreis * anzahlfahrkarten;
	    
    
		
	    //Ticketgrenze
	   
		while (anzahlfahrkarten < TicketMinimum || anzahlfahrkarten > TicketMaximum) {
            System.out.println(">> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
            System.out.println("");
            anzahlfahrkarten = fahrkartenanzahl.nextDouble();
            System.out.println("Anzahl der Tickets: " + anzahlfahrkarten);
            gesamtpreis = fahrscheinpreis * anzahlfahrkarten;
        
        
	    	
		
	    }
	    
	    return gesamtpreis;
    }

   
    public static double fahrkartenBezahlen(double gesamtpreis, double eingezahlterGesamtbetrag, double eingeworfeneM�nze, double r�ckgabebetrag) {
        double eingeworfenemuenze;
        
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < gesamtpreis) {
            System.out.format("Noch zu zahlen: %4.2f � %n", (gesamtpreis - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
            
        }
        
        
        
        return eingezahlterGesamtbetrag - gesamtpreis;
       
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {

        if (rueckgabebetrag > 0.0) {
            System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-M�zen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
            
            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
	                + "Wir w�nschen Ihnen eine gute Fahrt.\n");
        }
    }

    //Fahrkartenautomat mit fertigen Arrays, wie in A5.3 gefordert. :)
    
   
}