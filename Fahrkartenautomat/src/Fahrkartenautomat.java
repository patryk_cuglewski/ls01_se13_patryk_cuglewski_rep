﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       
       double eingezahlterGesamtbetrag;
       double gesamtpreis;
       
       gesamtpreis = fahrkartenbestellungErfassen ();
       eingezahlterGesamtbetrag = fahrkartenBezahlen (gesamtpreis);
       fahrkartenAusgeben ();
       rueckgeldAusgabe ( eingezahlterGesamtbetrag, gesamtpreis);
       tastatur.close();
       
    }

    
    public static double fahrkartenbestellungErfassen (){
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	double  anzahlTickets = 0;
    	// Tickets
        //-----------
        System.out.print("Ticketspreis (EURO): ");
        zuZahlenderBetrag = tastatur.nextDouble();

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt(); 
        
        double gesamtpreis = zuZahlenderBetrag * anzahlTickets;
        tastatur.close();
        return gesamtpreis;
    }
  
    public static double fahrkartenBezahlen (double gesamtpreis){
    Scanner tastatur = new Scanner(System.in);
    // Geldeinwurf
    // -----------
    double eingezahlterGesamtbetrag = 0;
   	double zuZahlenderBetrag = 0;
	double anzahlTickets = 0;
	double eingeworfeneMünze = 0;
    while(eingezahlterGesamtbetrag < gesamtpreis)
    {
 	   System.out.printf("Noch zu zahlen: ");
 	   System.out.printf("%.2f",gesamtpreis - eingezahlterGesamtbetrag);
 	   System.out.println(" Euro");
 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
 	   eingeworfeneMünze = tastatur.nextDouble();
       eingezahlterGesamtbetrag += eingeworfeneMünze;
       tastatur.close();
        
       
    }
    return eingezahlterGesamtbetrag;
    }
    public static void fahrkartenAusgeben (){
    
    // Fahrscheinausgabe
    // -----------------
    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    System.out.println("\n\n");
    
    }
    public static void rueckgeldAusgabe (double eingezahlterGesamtbetrag, double gesamtpreis ){
    
    	
    
    // Rückgeldberechnung und -Ausgabe
    // -------------------------------
    double rückgabebetrag = eingezahlterGesamtbetrag - gesamtpreis;
    if(rückgabebetrag > 0.0)
    {
 	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" ,rückgabebetrag);
 	   System.out.println(" EURO");
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println(" EURO");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        }
    }

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
    

 }
    
}

		//Double "gesamtpreis" fasst die Rechnung "zuZahlenderBetrag * anzahlTickets" zusammen und hilft uns die Anzahl der Tickets in die Rechnung zu integrieren.
		//Ich habe mich für die Anzahl der Tickets für einen int Datentyp entschieden, da man nur ganze Tickets bestellen kann und nicht 2 und ein halbes.
		//Der "gesamtpreis" wurde in der Rechnung für den "Geldeinwurf" und "Rückgeldberechnung und -Ausgabe" integriert.